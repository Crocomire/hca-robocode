# Solutis Robocode
### Introdução
- Sobre o robô e código
- Pontos fortes e fracos
- Considerações finais

## Sobre

### O robô
Simples e desenvolvido em **C#**, com movimentos básicos e previsiveis, possui uma baixa taxa de disparo porém de alto dano, e um ângulo de disparo de 360 graus. 

### O código
Ao Inicializar, Seu corpo dará um giro de 90° para a esquerda, e sua arma dará um giro de 720° numa tentativa de acertar qualquer robô que ainda esteja se posicionando no campo, escanenado por alvos ou tentando encurtar sua distância com o Artificer. Logo após ele entrará em um loop que irá:
- Avançar numa linha reta por 500 pixels
- Girar o canhão em 360° e disparar em qualquer alvo a vista
- Girar seu corpo em 50°
- Recuar numa linha Reta por 500 pixels
- Girar seu corpo em 50°
- Girar o canhão em 360° e disparar em qualquer alvo a vista

E por último, uma tentativa de comandar o robô para atirar somente quando a temperatura da arma estiver em 0, se esta condição for atendida, ele efetuará um disparo com a maior potência possível.


## Pontos fortes e fracos

Este robô tem uma maior performance contra robôs igualmente simples ou que não se movem tanto. Ou robôs semelhantes ao **sample RamFire**.
Sua performance diminue contra robôs que possuem maior mobilidade.

#### Fortes
- Alto dano
- Boa mobilidade
- Forte contra alvos estáticos ou de baixa mobilidade
- Capaz de fugir e esquivar de disparos inimigos

#### Fracos
- Previsivel
- Movimentação Linear
- Vunerável enquanto dispara
- Fraco contra alvos de maior mobilidade
- Disparos fáceis de esquivar
- Baixa velocidade de disparo
- Pode facilmente ficar preso entre um inimigo e uma parede

Apesar da maior quantidade de pontos fracos, Artificer ainda é capaz de vencer em uma batalha, mesmo sendo encurralado, seu alto dano o dará vantagem em certas situações, e estando em movimento quase constante, o ajuda a minimizar o dano recebido se atacado.

## Considerações finais

Gostei de ter tido a oportunidade para utilizar C#. Apesar de nunca ter usando a línguagem e ter tido algumas dificuldades, foi proveitoso essa introdução.