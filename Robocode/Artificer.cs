//This Robot should be great against stationary targets, but efficiency decreases against moving targets.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
//Access to the public Robocode API.
using Robocode;


namespace HCA

{
    //Name of my Robot and the type of the robot, which is *Robot*.
    public class Artificer : Robot
    {
       
        public override void Run()
        {
            //Sets the color of the robot's body, gun, radar, bullet, and scan arc in the same time. 
            SetColors(Color.Black, Color.OrangeRed, Color.Gold, Color.DarkRed, Color.Black);
            //--Inicialization of the Robot--
            TurnLeft(Heading - 90);
            TurnGunRight(720);


            while (true)
            {

                //Move 500 pixels forward.
                Ahead(500);
                //Spin the gun to shoot any target.
                TurnGunRight(360);
                //50 degrees turn to the right.
                TurnRight(50);
                //Move 500 pixels backward.
                Ahead(-500);
                //Turns 50 degrees right after moving backwards.
                TurnRight(50);
                //Another 360 spin to shoot any target.
                TurnGunRight(360);
                //This movement pattern should make the Robot Dodge most of the bullets.
            }
        }

        private double gunHeat;


        public double GetGunHeat()
        {
            return gunHeat;
        }

        public override void OnScannedRobot(ScannedRobotEvent e)
        {

            // Fire a bullet with maximum power if the gun is ready.
            if (GetGunHeat() == 0)
            {
                FireBullet(Rules.MAX_BULLET_POWER);

                //Though i am not sure if this is necessary.
                //By the time the robot finishes moving, the gun should be cool enough to shoot, but i will leave it anyways.

            }

        }

    }

}
